class DiaryModel {

  DiaryModel(
      {required this.id,
      required this.userId,
      required this.title,
      required this.content,
      required this.dateTime});

  late String id;
  late String userId;
  late String title;
  late String content;
  late String dateTime;

  DiaryModel.fromMap(Map<String, dynamic> data) {
    id = data["id"];
    userId = data["userId"];
    title = data["title"];
    content = data["content"];
    dateTime = data["dateTime"];
  }

  Map<String, dynamic> toJson() => {
        "id": id,
        "userId": userId,
        "title": title,
        "content": content,
        "dateTime": dateTime,
      };
}
