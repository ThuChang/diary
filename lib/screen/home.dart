
import 'package:diary/screen/diary/diary_screen.dart';
import 'package:diary/screen/user/account_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../main.dart';
class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});
  // static const String id = "home_screen";

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  int _currentIndex = 0;
  // UserModel? _userModel;
  bool isLoading=false;
  final List<Widget> _children = const [
    DiaryScreen(),
    AccountScreen(),
  ];
  void onTabTapped(int index) {
    setState(() {
      _currentIndex = index;
    });
  }
  // void initState() async {
  //   loadUser();
  //   _userModel = context.read<UserState>().userModel;
  //
  //   super.initState();
  // }
  //
  // Future<void> loadUser() async {
  //   isLoading = true;
  //   await context.read<UserState>().loadUserModel().then((value) {
  //     setState(() {
  //       isLoading = false;
  //     });
  //   });
  // }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
            key: navigatorKey,
            body: SafeArea(child: _children[_currentIndex]),
            bottomNavigationBar: BottomNavigationBar(
              items: const <BottomNavigationBarItem>[
                BottomNavigationBarItem(
                  icon: Icon(Icons.edit_note),
                  label: 'Jobs',
                ),

                BottomNavigationBarItem(
                  icon: Icon(Icons.manage_accounts),
                  label: 'Account',
                )
              ],
              currentIndex: _currentIndex,
              selectedItemColor: Color(0xFFFFB900),
              unselectedItemColor: Colors.black,
              onTap: onTabTapped,
            ),
          );

  }
}
