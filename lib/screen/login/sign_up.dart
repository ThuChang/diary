import 'dart:async';

import 'package:diary/screen/login/sign_in.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../common/alert.dart';
import '../../model/user_model.dart';
import '../repository/user_repo.dart';
import '../repository/user_state.dart';
class SignUpScreen extends StatefulWidget {
  const SignUpScreen({Key? key}) : super(key: key);
  // static const String id = "signup_screen";


  @override
  State<SignUpScreen> createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> {
  final UserRepository userRepository = UserRepository();
  bool isLoding = false;
  TextEditingController _username = TextEditingController();
  TextEditingController _password = TextEditingController();
  TextEditingController _email = TextEditingController();
  TextEditingController _address = TextEditingController();
  bool regexEmail = false, regexPassword = false,regexUser = false,regexAddress = false;
  StreamController<bool> buttonLogin = StreamController()..add(false);

  Future register()async{
    try {
      UserModel u = UserModel(
          id: "",
          username: _username.text,
          email: _email.text,
          address: _address.text);
      isLoding = true;
      await context.read<UserState>().register(u,_password.text).then((value) {
        Navigator.pop(context);
        isLoding = false;
      });
    } catch (e) {
      alert(context, 'alertLogin');
    }
  }
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: ListView(
        children: [
          // Image.asset("assets/images/img.png"),
          SizedBox(
            height: 30,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            child: TextFormField(
              controller: _email,
              decoration: InputDecoration(
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10.0),
                ),
                labelText: 'Email',
              ),
              keyboardType: TextInputType.emailAddress,
              autovalidateMode: AutovalidateMode.onUserInteraction,
              validator: (value) {
                // Check if this field is empty
                if (value == null || value.isEmpty) {
                  regexEmail = false;
                  return 'This field is required';
                } else
                  regexEmail = true;

                // using regular expression
                if (!RegExp(r'\S+@\S+\.\S+').hasMatch(value)) {
                  regexEmail = false;
                  return "Please enter a valid email address";
                } else
                  regexEmail = true;
                buttonLogin.sink.add(true);
                if(regexEmail && regexPassword && regexUser && regexAddress) buttonLogin.sink.add(true);
                else buttonLogin.sink.add(false);
                // the email is valid
                return null;
              },
            ),
          ),
          SizedBox(
            height: 30,
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(10, 0, 10, 20),
            child: TextFormField(
              controller: _password,
              obscureText: true,
              autovalidateMode: AutovalidateMode.onUserInteraction,
              decoration: InputDecoration(
                // prefixIcon: ImageIcon('assets/icon/pass.png' as ImageProvider<Object>?),
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10.0),
                ),

                labelText: 'Password',
              ),
              //keyboardType: TextInputType.visiblePassword,

              validator: (value) {
                // Check if this field is empty
                if (value == null || value.isEmpty) {
                  regexPassword = false;
                  return 'This field is required';
                } else regexPassword = true;

                // using regular expression
                if (!RegExp(
                    r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~]).{8,}$')
                    .hasMatch(value)) {
                  regexPassword = false;
                  return "Please enter a valid email address";
                } else regexPassword = true;

                if(regexEmail && regexPassword && regexUser && regexAddress) buttonLogin.sink.add(true);
                else buttonLogin.sink.add(false);
                // the email is valid
                return null;
              },
            ),
          ),

          SizedBox(
            height: 30,
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(10, 0, 10, 20),
            child: TextFormField(
              controller: _username,
              autovalidateMode: AutovalidateMode.onUserInteraction,
              decoration: InputDecoration(
                // prefixIcon: ImageIcon('assets/icon/pass.png' as ImageProvider<Object>?),
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10.0),
                ),

                labelText: 'Username',
              ),
              //keyboardType: TextInputType.visiblePassword,

              validator: (value) {
                // Check if this field is empty
                if (value == null || value.isEmpty) {
                  regexUser = false;
                  return 'This field is required';
                } else regexUser = true;

                // using regular expression

                if(regexEmail && regexPassword && regexUser && regexAddress) buttonLogin.sink.add(true);
                else buttonLogin.sink.add(false);
                // the email is valid
                return null;
              },
            ),
          ),
          SizedBox(
            height: 30,
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(10, 0, 10, 20),
            child: TextFormField(
              controller: _address,
              autovalidateMode: AutovalidateMode.onUserInteraction,
              decoration: InputDecoration(
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10.0),
                ),

                labelText: 'Address',
              ),
              //keyboardType: TextInputType.visiblePassword,

              validator: (value) {
                // Check if this field is empty
                if (value == null || value.isEmpty) {
                  regexAddress = false;
                  return 'This field is required';
                } else regexAddress = true;

                // using regular expression

                if(regexEmail && regexPassword && regexUser && regexAddress) buttonLogin.sink.add(true);
                else buttonLogin.sink.add(false);
                // the email is valid
                return null;
              },
            ),
          ),

          StreamBuilder(
            stream: buttonLogin!.stream,
            builder: (_,snap){
              return InkWell(
                child: SizedBox(
                  height: 45,
                  child: DecoratedBox(
                    decoration: BoxDecoration(
                      color: snap.data! ? Color(0xFFFFB900) : Color(0xFF52514F),
                      border: Border.all(
                        color: const Color(0xFFEDE1CF),
                      ),
                      borderRadius:
                      const BorderRadius.all(Radius.circular(10)),
                    ),
                    child: const Center(
                      child: Text(
                        'Sign up',
                        style: TextStyle(
                          fontSize: 22,
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ),
                ),
                onTap: () async {
                  register();
                  // userRepository!.register(_email.text, _username.text, _password.text);
                },
              );
            },
          ),
          Center(
            child: TextButton(
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => SignInScreen()),
                );
              },
              child: Text(
                'Da co tai khoan?',
                style: TextStyle(
                  fontSize: 16,
                  color: Color(0xFF7F7667),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

