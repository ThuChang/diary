import 'dart:async';

import 'package:diary/screen/login/sign_up.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../common/alert.dart';
import '../home.dart';
import '../repository/user_repo.dart';
import '../repository/user_state.dart';


class SignInScreen extends StatefulWidget {
  const SignInScreen({Key? key}) : super(key: key);
  // static const String id = "signin_screen";



  @override
  State<SignInScreen> createState() => _SignInScreenState();
}

class _SignInScreenState extends State<SignInScreen> {
  final UserRepository userRepository = UserRepository();
  TextEditingController _username = TextEditingController();
  TextEditingController _password = TextEditingController();
  bool regexEmail = false, regexPassword = false;
  StreamController<bool> buttonLogin = StreamController()..add(false);

  Future<void> resetPassword(String email) async {
    try {
      await FirebaseAuth.instance.sendPasswordResetEmail(email: email);
      alert(context,'Một email đặt lại mật khẩu đã được gửi đến địa chỉ email của bạn.');
    } catch (e) {
      alert(context,'Không thể gửi email đặt lại mật khẩu: $e');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: ListView(
        children: [
          // Image.asset("assets/images/img.png"),
          Center(
            child: Text(
              "Hey there, welcome back!",
              style: TextStyle(
                fontSize: 22,
                fontWeight: FontWeight.w400,
              ),
            ),),
          Center(
              child: Text(
                "Sign in to continue with your email, facebook or google account.",
                style: TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.w400,
                ),
              )),
          SizedBox(
            height: 30,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            child: TextFormField(
              controller: _username,
              decoration: InputDecoration(
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10.0),
                ),
                labelText: 'Email',
              ),
              keyboardType: TextInputType.emailAddress,
              autovalidateMode: AutovalidateMode.onUserInteraction,
              validator: (value) {
                // Check if this field is empty
                if (value == null || value.isEmpty) {
                  regexEmail = false;
                  return 'This field is required';
                } else
                  regexEmail = true;

                // using regular expression
                if (!RegExp(r'\S+@\S+\.\S+').hasMatch(value)) {
                  regexEmail = false;
                  return "Please enter a valid email address";
                } else
                  regexEmail = true;
                buttonLogin.sink.add(true);
                if(regexEmail && regexPassword) buttonLogin.sink.add(true);
                else buttonLogin.sink.add(false);
                // the email is valid
                return null;
              },
            ),
          ),
          SizedBox(
            height: 30,
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(10, 0, 10, 20),
            child: TextFormField(
              controller: _password,
              obscureText: true,
              autovalidateMode: AutovalidateMode.onUserInteraction,
              decoration: InputDecoration(
                // prefixIcon: ImageIcon('assets/icon/pass.png' as ImageProvider<Object>?),
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10.0),
                ),

                labelText: 'Password',
              ),
              //keyboardType: TextInputType.visiblePassword,

              validator: (value) {
                // Check if this field is empty
                if (value == null || value.isEmpty) {
                  regexPassword = false;
                  return 'This field is required';
                } else regexPassword = true;

                // using regular expression
                if (!RegExp(
                    r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~]).{8,}$')
                    .hasMatch(value)) {
                  regexPassword = false;
                  return "Please enter a valid email address";
                } else regexPassword = true;

                if(regexEmail && regexPassword) buttonLogin.sink.add(true);
                else buttonLogin.sink.add(false);
                // the email is valid
                return null;
              },
            ),
          ),


          Center(
            child: TextButton(
              onPressed: () {
                resetPassword(_username.text);
              },
              child: Text(
                'Forgot Password',
                style: TextStyle(
                  fontSize: 16,
                  color: Color(0xFF7F7667),
                ),
              ),
            ),
          ),
          StreamBuilder(
            stream: buttonLogin!.stream,
            builder: (_,snap){
              return InkWell(
                child: SizedBox(
                  height: 45,
                  child: DecoratedBox(
                    decoration: BoxDecoration(
                      color: snap.data! ? Color(0xFFFFB900) : Color(0xFF52514F),
                      border: Border.all(
                        color: const Color(0xFFEDE1CF),
                      ),
                      borderRadius:
                      const BorderRadius.all(Radius.circular(10)),
                    ),
                    child: const Center(
                      child: Text(
                        'Sign in',
                        style: TextStyle(
                          fontSize: 22,
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ),
                ),
                onTap: () async {

                  context.read<UserState>().login(_username.text, _password.text, context);
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => HomeScreen()),
                  );
                },
              );
            },
          ),
          Center(
            child: TextButton(
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => SignUpScreen()),
                );
              },
              child: Text(
                'Chua co tai khoan?',
                style: TextStyle(
                  fontSize: 16,
                  color: Color(0xFF7F7667),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
