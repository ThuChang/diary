import '../../model/diary_model.dart';

class ListDiaryState {
  final bool isLoading;
  final List<DiaryModel>? listDiary;
  ListDiaryState({
    this.isLoading = false,
    this.listDiary,
  });
  ListDiaryState copyWith({
    bool? isLoading,
    List<DiaryModel>? listDiary,
  }) {
    return ListDiaryState(
      isLoading: isLoading ?? this.isLoading,
      listDiary: listDiary ?? this.listDiary,
    );
  }
}
