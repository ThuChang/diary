import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:diary/model/diary_model.dart';
import 'package:diary/model/user_model.dart';
import 'package:diary/screen/bloc/diary_list_state.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ListDiaryCubit extends Cubit<ListDiaryState> {
  ListDiaryCubit():super(ListDiaryState());
  final user = FirebaseAuth.instance.currentUser;
  void init() async {
    emit(state.copyWith(isLoading: true));
    final data = await getlistDiary();
    emit(state.copyWith(isLoading: false, listDiary: data));
  }

  Future<List<DiaryModel>?> getlistDiary() async {
    StreamController<List<DiaryModel>> controller =
    StreamController<List<DiaryModel>>();
    try {
      QuerySnapshot querySnapshot = await FirebaseFirestore.instance
          .collection('diary')
          .where("userId", isEqualTo: user!.uid)
          .get();
      List<DiaryModel> diaries = querySnapshot.docs.map((document) {
        return DiaryModel(
          id: document.id,
          userId: document["userId"],
          title: document["title"],
          content: document["content"],
          dateTime: document["dateTime"],
        );
      }).toList();

      return diaries;
    } catch (e) {
      print("Error getting diaries: $e");
      controller.addError("Error getting diaries");
      return null;
    }
  }
}
