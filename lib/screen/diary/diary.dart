import 'package:diary/model/diary_model.dart';
import 'package:flutter/material.dart';
class Diary extends StatefulWidget {
  final DiaryModel model;
  const Diary(this.model,{super.key});

  @override
  State<Diary> createState() => _DiaryState();
}

class _DiaryState extends State<Diary> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(15, 10, 15, 10),
      child: SizedBox(
        height: 100,
        child: DecoratedBox(
          decoration: BoxDecoration(
            color: const Color(0xFFFFFBFF),
            border: Border.all(
              color: const Color(0xFF7F7667),
            ),
            borderRadius: const BorderRadius.all(
                Radius.circular(15)),
          ),
          child: Row(
            children: [
              const SizedBox(width: 15,),
              TextButton(
                onPressed: () {},
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      widget.model.title,
                      style: const TextStyle(
                        fontSize: 20,
                        color: Color(0xFF53514D),
                        fontWeight: FontWeight.bold,
                      ),
                      overflow: TextOverflow.ellipsis,
                      textAlign: TextAlign.left,
                      maxLines: 1,
                    ),
                    const SizedBox(height: 2.5,),
                    Text(
                      widget.model.dateTime,
                      style: const TextStyle(
                        fontSize: 16,
                        color: Color(0xFF7F7667),

                      ),
                      overflow: TextOverflow.ellipsis,
                      textAlign: TextAlign.left,
                    ),
                    const SizedBox(height: 5,),
                    Text(
                      widget.model.content,
                      style: const TextStyle(
                        fontSize: 16,
                        color: Color(0xFF7F7667),
                      ),
                      overflow: TextOverflow.ellipsis,
                      textAlign: TextAlign.left,
                      maxLines: 2,
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}




