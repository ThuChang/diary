import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../../custom/custombutton.dart';
import '../../model/diary_model.dart';
class Update_Diary extends StatefulWidget {
  final DiaryModel model;
  const Update_Diary(this.model, {super.key});

  @override
  State<Update_Diary> createState() => _Update_DiaryState();
}

class _Update_DiaryState extends State<Update_Diary> {
  DiaryModel? d;
  final TextEditingController titleController = TextEditingController();
  final TextEditingController contentController = TextEditingController();

  void initState(){
    titleController.text = widget.model.title;
    contentController.text = widget.model.content;
    d = widget.model;
    super.initState();
  }

  Future<void> updateDiary() async {
    DocumentReference documentRef =
    FirebaseFirestore.instance.collection('diary').doc(widget.model.id);
    d!.title = titleController.text;
    d!.content = contentController.text;
    d!.dateTime = DateFormat('dd/MM/yyyy').format(DateTime.now());

    await documentRef.update(d!.toJson()).then((value) {
      showDialog(
          context: context,
          builder: (BuildContext ctx) {
            return Material(
              color: Colors.transparent,
              child: Center(
                child: Container(
                  decoration: BoxDecoration(
                      color: Colors.white,
                      border: Border.all(color: Color(0xFF7F7667)),
                      borderRadius: BorderRadius.circular(20)),
                  padding: const EdgeInsets.all(30),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      const Text("Update Compelte!!!!"),
                      const SizedBox(
                        height: 20,
                      ),
                      GestureDetector(
                        onTap: () {
                          Navigator.of(context)
                              .popUntil((route) => route.isFirst);
                        },
                        child: Container(
                          padding: EdgeInsets.all(10),
                          decoration: BoxDecoration(
                              color: Color(0xFFFFB900),
                              border: Border.all(color: Color(0xFF7F7667)),
                              borderRadius: BorderRadius.circular(20)),
                          child: Text("BackHome"),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            );
          });
    });
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text("Update Diary"),
        backgroundColor: Color(0xFFFFB900),
        elevation: 0,
        actions: [
          IconButton(
              onPressed: () {
                FirebaseFirestore.instance
                    .collection('diary')
                    .doc(widget.model.id)
                    .delete()
                    .then((value) {
                  showDialog(
                      context: context,
                      builder: (BuildContext ctx) {
                        return Material(
                          color: Colors.transparent,
                          child: Center(
                            child: Container(
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  border: Border.all(color: Color(0xFF7F7667)),
                                  borderRadius: BorderRadius.circular(20)),
                              padding: const EdgeInsets.all(30),
                              child: Column(
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  const Text("Delete Compelte!!!!"),
                                  const SizedBox(
                                    height: 20,
                                  ),
                                  GestureDetector(
                                    onTap: () {
                                      Navigator.of(context)
                                          .popUntil((route) => route.isFirst);
                                    },
                                    child: Container(
                                      padding: EdgeInsets.all(10),
                                      decoration: BoxDecoration(
                                          color: Colors.amber,
                                          border:
                                          Border.all(color: Color(0xFF7F7667)),
                                          borderRadius:
                                          BorderRadius.circular(20)),
                                      child: Text("BackHome"),
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                        );
                      });
                });
              },
              icon: Icon(Icons.delete))
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Column(
          children: [
            TextFormField(
              controller: titleController,
              decoration: InputDecoration(labelText: 'Title'),
            ),
            TextFormField(
              controller: contentController,
              decoration: InputDecoration(labelText: 'Content'),
              maxLines: 35,
            ),
            SizedBox(height: 10,),
            CustomButton(
              onPressed: () {
                updateDiary();
              },
              text: 'Update',
            ),
          ],
        ),
      ),
    );
  }
}
