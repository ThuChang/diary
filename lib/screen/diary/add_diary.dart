import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:diary/custom/custombutton.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import '../../common/alert.dart';
import '../../model/diary_model.dart';

class AddDiaryEntryScreen extends StatefulWidget {
  const AddDiaryEntryScreen({Key? key}) : super(key: key);

  @override
  State<AddDiaryEntryScreen> createState() => _AddDiaryEntryScreenState();
}

class _AddDiaryEntryScreenState extends State<AddDiaryEntryScreen> {
  final user = FirebaseAuth.instance.currentUser;
  final titleController = TextEditingController();
  final contentController = TextEditingController();

  String email = "";

  late String dateTime = DateFormat('dd/MM/yyyy').format(DateTime.now());

  @override
  void initState() {
    email = user!.email ?? "thuchang12a3@gmail.com";
    super.initState();
  }

  Future<void> addDiary() async {
    bool isPush = true;

    if (titleController.text.trim().isEmpty) {
      isPush = false;
      alert(context, "Name chưa điền kìa bạn ơi");
    }
    if (contentController.text.trim().isEmpty) {
      isPush = false;
      alert(context, "Date of birthday chưa điền kìa bạn iu ơi");
    }

    if (isPush) {
      DiaryModel c = DiaryModel(
        id: '',
        userId: user!.uid,
        title: titleController.text,
        content: contentController.text,
        dateTime: dateTime,
      );
      FirebaseFirestore.instance
          .collection('diary')
          .add(c.toJson())
          .then((value) {
        showDialog(
            context: context,
            builder: (BuildContext ctx) {
              return Material(
                color: Colors.transparent,
                child: Center(
                  child: Container(
                    decoration: BoxDecoration(
                        color: Colors.white,
                        border: Border.all(color: Color(0xFF7F7667)),
                        borderRadius: BorderRadius.circular(20)),
                    padding: const EdgeInsets.all(30),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        const Text("Push Compelte!!!!"),
                        const SizedBox(
                          height: 20,
                        ),
                        GestureDetector(
                          onTap: () {
                            Navigator.of(context)
                                .popUntil((route) => route.isFirst);
                          },
                          child: Container(
                            padding: EdgeInsets.all(10),
                            decoration: BoxDecoration(
                                color: Color(0xFFFFB900),
                                border: Border.all(color: Color(0xFF7F7667)),
                                borderRadius: BorderRadius.circular(20)),
                            child: Text("BackHome"),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              );
            });
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Add Diary Entry')),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          children: <Widget>[
            TextField(
              controller: titleController,
              decoration: InputDecoration(labelText: 'Title'),
            ),
            TextField(
              controller: contentController,
              decoration: InputDecoration(labelText: 'Content'),
            ),
            SizedBox(
              height: 20,
            ),
            CustomButton(
              onPressed: () {
                addDiary();
              },
              text: 'Save Entry',
            ),
            SizedBox(
              height: 20,
            ),
          ],
        ),
      ),
    );
  }
}
