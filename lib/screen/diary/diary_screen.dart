import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:diary/model/diary_model.dart';
import 'package:diary/screen/bloc/diary_list_bloc.dart';
import 'package:diary/screen/bloc/diary_list_state.dart';
import 'package:diary/screen/diary/diary_detail.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../model/user_model.dart';
import '../repository/user_state.dart';
import 'package:provider/provider.dart';
import 'add_diary.dart';
class DiaryScreen extends StatefulWidget {
  const DiaryScreen({Key? key}) : super(key: key);

  @override
  State<DiaryScreen> createState() => _DiaryScreenState();
}

class _DiaryScreenState extends State<DiaryScreen> {
  bool isLoading = false;
  final user = FirebaseAuth.instance.currentUser;
  late ListDiaryCubit _cubit;
  UserModel? _userModel;
  @override
  void initState() {
    loadUser();
    _userModel = context.read<UserState>().userModel;
    _cubit = ListDiaryCubit()..init();
    super.initState();
  }

  Future<void> loadUser() async {
    isLoading = true;
    await context.read<UserState>().loadUserModel().then((value) {
      setState(() {
        isLoading = false;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:Builder(builder:(context) {
        if (isLoading)
          return const Center(
            child: CircularProgressIndicator(),
          );
          return Container(
            margin: const EdgeInsets.only(left: 13, right: 13, top: 30),
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text("Hello, ${context.read<UserState>().userModel.username}"),
                    IconButton(
                        onPressed: () {
                          print(user!.uid);
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => AddDiaryEntryScreen()),
                          );
                        },
                        icon: const Icon(Icons.add)),
                  ],
                ),
                Expanded(
                  child: BlocBuilder<ListDiaryCubit,ListDiaryState>(
                    bloc: _cubit,
                    builder: (context,state) {
                      if (state.isLoading) {
                        return const Center(
                          child: CircularProgressIndicator(),
                        );
                      }
                      if (!state.isLoading && state.listDiary == null) {
                        return Center(
                          child: CircularProgressIndicator(),
                        );
                      }
                      if (!state.isLoading && state.listDiary!.isEmpty) {
                        return Center(
                          child: Text('No data available'),
                        );
                      }

                      return ListView.builder(
                          shrinkWrap: true,
                          itemCount: state.listDiary?.length,
                          itemBuilder: (context, index) {

                            return InkWell(
                              child: Padding(
                                padding: const EdgeInsets.fromLTRB(15, 10, 15, 10),
                                child: SizedBox(
                                  height: 100,
                                  child: DecoratedBox(
                                    decoration: BoxDecoration(
                                      color: const Color(0xFFFFFBFF),
                                      border: Border.all(
                                        color: const Color(0xFF7F7667),
                                      ),
                                      borderRadius: const BorderRadius.all(
                                          Radius.circular(15)),
                                    ),
                                    child: Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            state.listDiary![index].title,
                                            style: const TextStyle(
                                              fontSize: 20,
                                              color: Color(0xFF53514D),
                                              fontWeight: FontWeight.bold,
                                            ),
                                            overflow: TextOverflow.ellipsis,
                                            textAlign: TextAlign.left,
                                            maxLines: 1,
                                          ),
                                          const SizedBox(height: 2.5,),
                                          Text(
                                            state.listDiary![index].dateTime,
                                            style: const TextStyle(
                                              fontSize: 16,
                                              color: Color(0xFF7F7667),

                                            ),
                                            overflow: TextOverflow.ellipsis,
                                            textAlign: TextAlign.left,
                                          ),
                                          const SizedBox(height: 5,),
                                          Flexible(
                                            child: Text(
                                              state.listDiary![index].content,
                                              style: const TextStyle(
                                                fontSize: 16,
                                                color: Color(0xFF7F7667),
                                              ),
                                              overflow: TextOverflow.ellipsis,
                                              textAlign: TextAlign.left,
                                              maxLines: 2,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              onTap: (){
                                print(user!.uid);
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => DiaryDetail(state.listDiary![index])),
                                );
                              },
                            );
                          });
                    },
                  ),
                ),
              ],
            ),
          );
        }),

    );
  }
}
