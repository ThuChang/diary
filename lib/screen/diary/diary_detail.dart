import 'package:diary/screen/diary/update_diary.dart';
import 'package:flutter/material.dart';

import '../../custom/custombutton.dart';
import '../../model/diary_model.dart';

class DiaryDetail extends StatefulWidget {
  final DiaryModel model;
  const DiaryDetail(this.model, {super.key});

  @override
  State<DiaryDetail> createState() => _DiaryDetailState();
}

class _DiaryDetailState extends State<DiaryDetail> {
  DiaryModel? d;
  final TextEditingController titleController = TextEditingController();
  final TextEditingController contentController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(widget.model.title),
        backgroundColor: Color(0xFFFFB900),
        elevation: 0,
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Stack(
          children: [
            Positioned(
              child: ListView(
                children: [
                  Text(widget.model.content),
                  SizedBox(
                    height: 55,
                  ),
                ],
              ),
            ),
            Positioned(
              bottom: 0,
              child: CustomButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => Update_Diary(widget.model)),
                  );
                },
                text: 'Edit',
              ),
            ),
          ],
        ),
      ),
    );
  }
}
