import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';

import '../../common/alert.dart';
import '../../custom/custombutton.dart';

class ChangePasswordScreen extends StatefulWidget {
  @override
  _ChangePasswordScreenState createState() => _ChangePasswordScreenState();
}

class _ChangePasswordScreenState extends State<ChangePasswordScreen> {
  //final _auth = FirebaseAuth.instance;
  final _formKey = GlobalKey<FormState>();
  final _passwordController = TextEditingController();
  final _newPasswordController = TextEditingController();
  final user = FirebaseAuth.instance.currentUser;
  void _changePassword() async {
    try {

      final email = user!.email.toString();
      final oldPassword = _passwordController.text;
      final newPassword = _newPasswordController.text;
      // Đăng nhập lại để xác minh mật khẩu cũ
      final credential = EmailAuthProvider.credential(email: email, password: oldPassword);
      await user?.reauthenticateWithCredential(credential);
      // Thay đổi mật khẩu
      await user?.updatePassword(newPassword);

      alert(context,'Mật khẩu đã được thay đổi.');
    } catch (e) {
      alert(context, 'Không thể thay đổi mật khẩu: $e');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xFFFFB900),
        title: Text('Thay đổi mật khẩu'),
      ),
      body: Builder(
        builder: (context) {
          return Center(
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: Form(
                key: _formKey,
                child: Column(
                  children: <Widget>[
                    TextFormField(
                      controller: _passwordController,
                      decoration: InputDecoration(labelText: 'Mật khẩu cũ'),
                      validator: (value) {
                        if (value!.isEmpty) {
                          return 'Vui lòng nhập mật khẩu cũ.';
                        }
                        return null;
                      },
                    ),
                    TextFormField(
                      controller: _newPasswordController,
                      decoration: InputDecoration(labelText: 'Mật khẩu mới'),
                      validator: (value) {
                        if (value!.isEmpty) {
                          return 'Vui lòng nhập mật khẩu mới.';
                        }
                        return null;
                      },
                    ),
                    SizedBox(height: 16.0),
                    CustomButton(
                      onPressed: () async{
                        if (_formKey.currentState!.validate()) {
                          _changePassword();

                        }
                      },
                      text:'Thay đổi mật khẩu',
                    ),
                  ],
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}
