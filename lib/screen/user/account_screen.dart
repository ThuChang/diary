import 'package:diary/screen/user/update_account.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../model/user_model.dart';
import '../repository/user_repo.dart';
import '../repository/user_state.dart';
import 'change_pass.dart';

class AccountScreen extends StatefulWidget {
  const AccountScreen({Key? key}) : super(key: key);

  @override
  State<AccountScreen> createState() => _AccountScreenState();
}

class _AccountScreenState extends State<AccountScreen> {
  final UserRepository userRepository = UserRepository();
  UserModel? _userModel;
  bool isLoading = false;
  final user = FirebaseAuth.instance.currentUser;

  @override
  void initState() {
    _userModel = context.read<UserState>().userModel;

    super.initState();
  }



  Future<void> logoutUser() async {
    isLoading = true;
    await context.read<UserState>().logout().then((value) {
      setState(() {
        isLoading = false;
      });
    });
  }
  @override
  Widget build(BuildContext context) {
    return Builder(builder: (context) {
      if (isLoading) return const CircularProgressIndicator();
      return Container(
        margin: const EdgeInsets.only(top: 30, left: 13, right: 13),
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          const Text(
            "Profile",
            style: TextStyle(fontSize: 30),
          ),
          const SizedBox(
            height: 20,
          ),
          Container(
            padding: const EdgeInsets.symmetric(horizontal: 30),
            height: MediaQuery.of(context).size.height * 0.15,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
                color: Colors.white,
                border: Border.all(color: Color(0xFF7F7667))),
            child: Row(children: [
              const CircleAvatar(
                radius: 30,
                child: Icon(Icons.person),
              ),
              const SizedBox(
                width: 20,
              ),
              Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(_userModel!.username),
                      const SizedBox(
                        height: 20,
                      ),
                      Text(user!.email!)
                    ],
                  ))
            ]),
          ),
          Expanded(
            child: Container(
              padding: const EdgeInsets.only(
                  left: 13, right: 13, top: 20, bottom: 20),
              decoration: const BoxDecoration(
                  border: Border(bottom: BorderSide(color: Color(0xFF7F7667)))),
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    //update
                    InkWell(
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => UpdateAcount()),
                        );
                      },
                      child: Container(
                        padding:
                        EdgeInsets.symmetric(horizontal: 10, vertical: 20),
                        decoration: const BoxDecoration(
                            border: Border(
                                bottom: BorderSide(color: Color(0xFF7F7667)))),
                        child: Row(
                          children: const [
                            Icon(Icons.edit),
                            SizedBox(
                              width: 10,
                            ),
                            Text("Update Account")
                          ],
                        ),
                      ),
                    ),
                    //changepass
                    InkWell(
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => ChangePasswordScreen()),
                        );
                      },
                      child: Container(
                        padding:
                        EdgeInsets.symmetric(horizontal: 10, vertical: 20),
                        decoration: const BoxDecoration(
                            border: Border(
                                bottom: BorderSide(color: Color(0xFF7F7667)))),
                        child: Row(
                          children: const [
                            Icon(Icons.vpn_key),
                            SizedBox(
                              width: 10,
                            ),
                            Text("Change Password")
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          InkWell(
            onTap: () async {
              // userRepository!.signOut();
              // await FirebaseAuth.instance.signOut();
              logoutUser();
            },
            child: Container(
              margin: const EdgeInsets.symmetric(vertical: 10),
              padding:
              EdgeInsets.only(left: 13, right: 13, top: 20, bottom: 20),
              decoration: BoxDecoration(
                  color: Color(0xFFFFB900),
                  border: Border.all(color: Color(0xFF7F7667)),
                  borderRadius: BorderRadius.circular(20)),
              child: Center(child: Text("Logout")),
            ),
          ),
        ]),
      );
    });
  }
}
