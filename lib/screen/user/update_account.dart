import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:diary/model/user_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../custom/custombutton.dart';
import '../repository/user_state.dart';
class UpdateAcount extends StatefulWidget {
  const UpdateAcount({Key? key}) : super(key: key);

  @override
  State<UpdateAcount> createState() => _UpdateAcountState();
}

class _UpdateAcountState extends State<UpdateAcount> {
  UserModel? _userModel;
  final TextEditingController controllerFullName = TextEditingController();
  final TextEditingController controllerAddress = TextEditingController();

  @override
  void initState() {
    _userModel = context.read<UserState>().userModel;
    controllerFullName.text = _userModel!.username;
    controllerAddress.text = _userModel!.address;
    super.initState();
  }

  Future<void> updateUser() async {
    DocumentReference documentRef =
    FirebaseFirestore.instance.collection('User').doc(_userModel!.id);
    _userModel!.address = controllerAddress.text;
    _userModel!.username = controllerFullName.text;
    await documentRef.update(_userModel!.toJson()).then((value) {
      showDialog(
          context: context,
          builder: (BuildContext ctx) {
            return Material(
              color: Colors.transparent,
              child: Center(
                child: Container(
                  decoration: BoxDecoration(
                      color: Colors.white,
                      border: Border.all(color: Color(0xFF7F7667)),
                      borderRadius: BorderRadius.circular(20)),
                  padding: const EdgeInsets.all(30),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      const Text("Update Compelte!!!!"),
                      const SizedBox(
                        height: 20,
                      ),
                      GestureDetector(
                        onTap: () {
                          Navigator.of(context)
                              .popUntil((route) => route.isFirst);
                        },
                        child: Container(
                          padding: EdgeInsets.all(10),
                          decoration: BoxDecoration(
                              color: Color(0xFFFFB900),
                              border: Border.all(color: Color(0xFF7F7667)),
                              borderRadius: BorderRadius.circular(20)),
                          child: Text("BackHome"),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            );
          });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      child: SafeArea(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            IconButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                icon: Icon(Icons.arrow_back)),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  TextField(
                    controller: controllerFullName,
                    decoration: const InputDecoration(
                      contentPadding:  EdgeInsets.all(10),
                      focusedBorder:  OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(15)),
                        borderSide: BorderSide(color: Color(0xFF7F7667)),
                      ),
                      enabledBorder:  OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(15)),
                        borderSide: BorderSide(color: Color(0xFF7F7667)),
                      ),
                      labelText: 'Username',
                      alignLabelWithHint: true,
                      labelStyle:  TextStyle(
                        fontSize: 13,
                        color: Color(0xff444548),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 30,
                  ),
                  TextField(
                    controller: controllerAddress,
                    decoration: const InputDecoration(
                      contentPadding:  EdgeInsets.all(10),
                      focusedBorder:  OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(15)),
                        borderSide: BorderSide(color: Color(0xFF7F7667)),
                      ),
                      enabledBorder:  OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(15)),
                        borderSide: BorderSide(color: Color(0xFF7F7667)),
                      ),
                      labelText: 'Address',
                      alignLabelWithHint: true,
                      labelStyle:  TextStyle(
                        fontSize: 13,
                        color: Color(0xff444548),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 50,
                  ),
                  CustomButton(
                      onPressed: () {
                        updateUser();
                      },
                      text:"Update"
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}