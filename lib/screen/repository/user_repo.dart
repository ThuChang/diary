import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';

import '../../common/alert.dart';

class UserRepository {
  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;

  // Đăng nhập bằng email và mật khẩu


  Future<void> register(String email,String username,String password) async {
    UserCredential userCredential =
    await FirebaseAuth.instance.createUserWithEmailAndPassword(
      email: email,
      password: password,
    );
    final user = FirebaseAuth.instance.currentUser;
    await user?.updateProfile(displayName: username);
    print(user!.displayName);

  }

  Future<void> login(String email, String password) async {

      UserCredential userCredential =
      await FirebaseAuth.instance.signInWithEmailAndPassword(
        email: email,
        password: password,
      );

  }

  // Đăng xuất
  Future<void> signOut() {
    return _firebaseAuth.signOut();
  }

  // Đổi mật khẩu
  Future<void> changePassword(String newPassword, String oldPassword) async {
    User? user = _firebaseAuth.currentUser;
    final credential = EmailAuthProvider.credential(email: user!.email.toString(), password: oldPassword);
    await user.reauthenticateWithCredential(credential);
    await user.updatePassword(newPassword);
  }

  // Cập nhật thông tin người dùng
  Future<void> updateProfile(String newUsername, String newEmail) async {

    User? user = _firebaseAuth.currentUser;
    if (user != null) {
      await user.updateProfile(displayName: newUsername);
      // await user.updateEmail(newEmail);
    }
  }

  // Lấy thông tin người dùng hiện tại

}
