import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

import '../../common/alert.dart';
import '../../model/user_model.dart';

class UserState extends ChangeNotifier {
  User? user;
  // UserModel userModel = UserModel(id: '1233', username: 'thuchang12a3@gmail.com', email: 'qwdwd', address: 'fcdcdcd');
  late UserModel userModel  ;
  UserState() {
    userModel = UserModel(id: '', username: '', email: '', address: '');
  }
  bool isLogin = false;
  String documentid = "";

  Future<void> initData() async {
    user = FirebaseAuth.instance.currentUser;
    documentid = user == null ? "" : user!.uid;
    isLogin = user == null ? false : true;
  }

  Future<void> loadUserModel() async {
    if (user != null) {
      QuerySnapshot querySnapshot = await FirebaseFirestore.instance
          .collection('User')
          .where("email", isEqualTo: user!.email)
          .get();
      if (querySnapshot.docs.isNotEmpty) {
        DocumentSnapshot document = querySnapshot.docs.first;
        userModel = UserModel(
          id: document.id,
          username: document["username"],
          email: document["email"],
          address: document["address"],
        );
      } else {
        print("Không tìm thấy tài liệu phù hợp");
      }
    }
  }

  Future<void> register(UserModel u,String password) async {
    UserCredential userCredential =
    await FirebaseAuth.instance.createUserWithEmailAndPassword(
      email: u.email,
      password: password,
    );
    user = userCredential.user;
    await FirebaseFirestore.instance
        .collection('User')
        .doc(user!.uid)
        .set(u.toJson())
        .then((value) {
      isLogin = user == null ? false : true;
      notifyListeners();
    });
  }

  Future<void> login(String email, String password, BuildContext ctx) async {
    try {
      UserCredential userCredential =
          await FirebaseAuth.instance.signInWithEmailAndPassword(
        email: email,
        password: password,
      );
      user = userCredential.user;
      isLogin = user == null ? false : true;
      notifyListeners();
    } on FirebaseAuthException catch (e) {
      if (e.code == 'user-not-found') {
        alert(ctx, "khong thay user");
      } else if (e.code == 'wrong-password') {
        alert(ctx, "sai mat khau");
      }
      alert(ctx, "Khong the dang nhap");

    }
  }

  Future logout() async {

    await FirebaseAuth.instance.signOut();
    isLogin = false;
    notifyListeners();
  }
}
